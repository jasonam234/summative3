SELECT d.DEPARTEMENT_ID as "Departement ID", d.DEPARTEMENT_NAME as "Departement Name", YEAR(e.HIRE_DATE) as "Year", COUNT(e.DEPARTEMENT_ID) as  "Total Employee" 
FROM departement as d
JOIN employee as e ON e.DEPARTEMENT_ID = d.DEPARTEMENT_ID
GROUP BY  d.DEPARTEMENT_ID, d.DEPARTEMENT_NAME;