SELECT FIRST_NAME as "First Name", LAST_NAME as "Last Name", EMAIL as  "Email", PHONE_NUMBER as "Phone Number", HIRE_DATE as "Hire Date" , SALARY as "Salary", COMMISSION_PCT as "Commission Percent"
FROM employee
WHERE MONTH(`HIRE_DATE`) = 8;