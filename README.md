# summative3

### Summative 1

Student table data

![11](./images/11.PNG)

Lesson table data

![12](./images/12.PNG)

Score table data

![13](./images/13.PNG)



### Summative 2

Query to show student name, lesson name & score

![21](./images/21.PNG)



### Summative 3

Employee table data

![21](./images/31.PNG)



### Summative 4

Show employee that hired in August

![41](./images/41.PNG)



### Summative 5

Show employee that hired between 01-06-1987 to 30-07-1987

![51](./images/51.PNG)

**NOTES : Create departement table first to use employee table**



### Summative 6

Departement table data

![61](./images/61.PNG)



### Summative 7

Get total employee from each departement

![71](./images/71.PNG)
