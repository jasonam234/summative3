-- Create database named summativedb 
CREATE DATABASE summativedb;
USE summativedb;

-- Table creation starts here 
CREATE TABLE Student(
	ID 			INTEGER NOT NULL UNIQUE AUTO_INCREMENT,
    NAME 		VARCHAR(30),
    SURNAME 	VARCHAR(30),
    BIRTHDATE	DATE,
    GENDER 		VARCHAR(30),
    
    PRIMARY KEY(ID)
);
SELECT * FROM Student;

CREATE TABLE Lesson(
	ID 			INTEGER NOT NULL UNIQUE AUTO_INCREMENT,
    NAME 		VARCHAR(30),
    LEVEL 		INTEGER,
    
    PRIMARY KEY(ID)
);
SELECT * FROM Lesson;

CREATE TABLE Score(
	ID 			INTEGER NOT NULL UNIQUE AUTO_INCREMENT,
    STUDENT_ID 	INTEGER,
    LESSON_ID 	INTEGER,
    SCORE 		INTEGER,
    
    PRIMARY KEY(ID),
    FOREIGN KEY(STUDENT_ID) REFERENCES Student(ID),
    FOREIGN KEY(LESSON_ID) REFERENCES Lesson(ID)
);
SELECT * FROM Score;

-- Data insertion starts here
INSERT INTO Student(NAME, SURNAME, BIRTHDATE, GENDER) VALUES
("Jason", "Mahalim", "1999-07-27", "Male"), 
("Jose", "Mahalim", "1999-07-27", "Male"), 
("John", "Doe", "2001-03-19", "Male"), 
("Jane", "Doe", "2002-01-26", "Female"), 
("Steven", "king", "2002-05-18", "Male");

INSERT INTO Lesson(NAME, LEVEL) VALUES
("Calculus", 3),
("Biology", 3),
("History", 2),
("Physics", 2),
("Algebra", 1);

INSERT INTO Score(STUDENT_ID, LESSON_ID, SCORE) VALUES
(1, 1, 80),
(1, 2, 70),
(2, 1, 75),
(2, 2, 80),
(3, 3, 90),
(4, 3, 100),
(4, 4, 40),
(5, 5, 0);