SELECT st.NAME as "Student First Name", st.SURNAME as "Student Last Name", st.BIRTHDATE as "Student Birth Date", st.GENDER as "Student Gender",le.NAME as "Lesson Name", sc.SCORE as "Score" FROM Score as sc
JOIN Student as st ON st.ID = sc.STUDENT_ID
JOIN Lesson as le ON le.ID = sc.LESSON_ID;